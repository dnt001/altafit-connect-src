<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login')->with('errorsLogin','');
})->name('screen');

Route::get('/login', function () {
    return view('auth.login')->with('errorsLogin','');
})->name('login');

Route::get('/reset', function () {
    return view('auth.passwords.email');
})->name('password.request');


Route::get('/home', 'HomeController@index')->name('home');
Route::post('/change-password', 'UserController@changePassword')->name('change.password');

Route::get('/cities', 'RestController@cities');
Route::get('/providences', 'RestController@providences');

Route::post('/cities', 'RestController@saveCities');
Route::post('/providences', 'RestController@saveProvidences');

Route::get('/directors', 'RestController@directors');
Route::get('/selected/{club}','RestController@selected')->name('selected');
Route::get('/coordinators', 'RestController@coordinators');
Route::post('/store-club','ClubController@storeClub')->name('clubs.store-club');
Route::post('/update-club/{id}','ClubController@updateClub')->name('clubs.update-club');
Route::post('/delete-club','ClubController@deleteClub')->name('clubs.delete-club');





// Route::group(['middleware' => ['permission:groups']], function () {
    Route::resource('groups', 'GroupsController');
// });

// Route::group(['middleware' => ['permission:permissions']],function(){
    Route::resource('permissions','PermissionsController');
// });
// // Route::resource('users', 'UserController');

Route::resource('users', 'UserController');


Route::resource('clubs', 'ClubController');

Route::get('imports','ClubController@imports')->name('imports.index');
Route::post('imports','ClubController@upload')->name('imports.upload');

Route::get('export','ClubController@export')->name('export');

Route::get('/active/{id}','RestController@active')->name('active');
Route::get('/unactive/{id}','RestController@unactive')->name('unactive');

Route::get('/active-club/{id}','ClubController@active')->name('active.club');
Route::get('/unactive-club/{id}','ClubController@unactive')->name('unactive.club');
Route::post('/login-altafit','LandingController@login')->name('login.altafit');
Route::post('/password-altafit','LandingController@reset')->name('reset.altafit');
Route::get('recovery/{token}','LandingController@recovery')->name('recovery');
Route::post('logout', 'LandingController@logout')->name('logout');

