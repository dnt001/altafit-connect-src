<?php

namespace App\Imports;

use Illuminate\Support\Str;

use App\Models\Providence;
use App\Models\Club;
use App\Models\Profile;
use App\Models\City;
use App\User;

use Auth;

use Maatwebsite\Excel\Concerns\ToModel;

class ClubImport implements ToModel
{
    /**
     * @param array $row
     *
     * @return User|null
     */
    public function model(array $row)
    {
      $cities         = City::pluck('id','name')->all();
      $providences    = Providence::pluck('id','name')->all();

      $clubs          = Club::pluck('id','uuid')->all();

      $directors      = User::role('Director Club')->pluck('id','email')->all();
      $coordinators   = User::role('Coordinador Club')->pluck('id','email')->all();
      

      if($row[2] != 'DNS'){

        if(!isset($clubs[$row[0]])){

          if(!isset($cities[$row[10]])){
            if(!is_null($row[10])){
              $city = City::create(['name' => $row[10]]);
              $city_id = $city->id;
            }else{
              $city_id = 0;
            }
          }else{
            $city_id = $cities[$row[10]];
          }

          if(!isset($providences[$row[11]])){
            if(!is_null($row[11])){
              $community = Providence::create(['name' => $row[11]]);
              $community_id = $community->id;
            }else{
              $community_id = 0;
            }
          }else{
            $community_id = $providences[$row[11]];
          }


          $password = Str::random(10);

          if(!isset($directors[$row[14]])){
            if(!is_null($row[14])){

              $director = new User();
              $director->name = $row[12];
              $director->email = $row[14];
              $director->first_login = 0;
              $director->password = bcrypt($password);
              $director->save();

              $director->assignRole('Director Club');

              $profile_director = new Profile();
              $profile_director->phone = $row[13];
              $profile_director->user_id = $director->id;
              $profile_director->save();

              $director_id = $director->id;
            }else{
              $director_id = null;
            }
          }else{
            $director_id = $directors[$row[14]];
          }

          $password_coordinator = Str::random(8);

          if(!isset($coordinators[$row[17]])){
            if(!is_null($row[17])){
              $coordinator = new User();
              $coordinator->name = $row[15];
              $coordinator->email = $row[17];
              $coordinator->first_login = 0;
              $coordinator->password = bcrypt($password_coordinator);
              $coordinator->save();

              $coordinator->assignRole('Director Club');

              $profile_coordinator = new Profile();
              $profile_coordinator->phone = $row[16];
              $profile_coordinator->user_id = $coordinator->id;
              $profile_coordinator->save();

              $coordinator_id = $coordinator->id;
            }else{
              $coordinator_id = null;
            }
          }else{
            $coordinator_id = $coordinators[$row[17]];
          }

          $club = new Club();
          $club->name = $row[5];
          $club->uuid = $row[0];
          $club->provis_id = $row[1];
          $club->dns = $row[2];
          $club->port = $row[3];
          $club->type = $row[4];
          $club->bussines_name = $row[6];
          $club->cif = $row[7];
          $club->address = $row[8];
          $club->postal_code = $row[9];
          $club->city_id = $city_id;
          $club->providence_id = $community_id;
          $club->director_id = $director_id;
          $club->coordinator_id = $coordinator_id;
          $club->save();

        } 
      }
    }

    
}