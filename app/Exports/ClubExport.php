<?php 

namespace App\Exports;

use App\Models\Club;
use DB;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ClubExport implements FromCollection, WithHeadings
{
    public function collection()
    {
    	$clubs =  Club::select('clubs.uuid','clubs.name','clubs.dns',
    							'clubs.port','clubs.type','clubs.bussines_name',
    							'clubs.cif','clubs.address','clubs.postal_code',
    							DB::raw('(select name from users where id = clubs.director_id) as Director'),
	    						DB::raw('(select name from users where id = clubs.coordinator_id) as Coordinador'),
	    						DB::raw('(select name from cities where id = clubs.city_id) as Ciudad'),
	    						DB::raw('(select name from providences where id = clubs.providence_id) as Provincia'))->get();
    	
        return $clubs;
    }

    public function headings(): array
    {
        return [
            'Código',
            'Centro',
            'DNS',
            'Puerto',
            'Tipo',
            'Razón Social',
            'CIF',
            'Dirección',
            'Código Postal',
            'Director',
            'Coordinador',
            'Ciudad',
            'Provincia'
        ];
    }
}