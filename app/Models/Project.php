<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Project
 * @package App\Models
 * @version March 27, 2019, 4:03 am UTC
 *
 * @property string name
 * @property string description
 * @property integer min_price
 * @property integer max_price
 * @property integer flag_price
 * @property integer total_price
 * @property integer payments
 * @property integer flag_payments
 */
class Project extends Model
{
    use SoftDeletes;

    public $table = 'projects';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
        'description',
        'min_price',
        'max_price',
        'flag_price',
        'total_price',
        'payments',
        'flag_payments'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'description' => 'string',
        'min_price' => 'integer',
        'max_price' => 'integer',
        'flag_price' => 'integer',
        'total_price' => 'integer',
        'payments' => 'integer',
        'flag_payments' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'description' => 'required',
        'min_price' => 'required',
        'max_price' => 'required',
        'payments' => 'required'
    ];

    
}
