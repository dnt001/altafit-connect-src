<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\City;
use App\Models\Providence;

use App\User;
/**
 * Class Club
 * @package App\Models
 * @version May 1, 2019, 4:57 pm UTC
 *
 * @property string name
 * @property string pv_sk
 * @property string pv_pk
 * @property string uuid
 * @property string provis_id
 * @property string dns
 * @property string port
 * @property string suffix
 * @property string type
 * @property string origin
 * @property string bussines_name
 * @property string cif
 * @property string address
 * @property string postal_code
 * @property string city
 * @property string providence
 * @property integer director_id
 * @property integer coordinator
 */
class Club extends Model
{
    use SoftDeletes;

    public $table = 'clubs';
    

    protected $dates = ['deleted_at'];

    protected $hidden = [
        'deleted_at', 'created_at','updated_at',
    ];


    protected $appends = ['city','providence','director'];


    public $fillable = [
        'name',
        'pv_sk',
        'pv_pk',
        'uuid',
        'provis_id',
        'dns',
        'port',
        'suffix',
        'type',
        'origin',
        'bussines_name',
        'cif',
        'address',
        'postal_code',
        'city_id',
        'providence_id',
        'director_id',
        'coordinator_id'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'pv_sk' => 'string',
        'pv_pk' => 'string',
        'uuid' => 'string',
        'provis_id' => 'string',
        'dns' => 'string',
        'port' => 'string',
        'suffix' => 'string',
        'type' => 'string',
        'origin' => 'string',
        'bussines_name' => 'string',
        'cif' => 'string',
        'address' => 'string',
        'postal_code' => 'string',
        'city_id' => 'string',
        'providence_id' => 'string',
        'director_id' => 'integer',
        'coordinator_id' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required',
        'pv_sk' => 'required',
        'pv_pk' => 'required',
        'uuid' => 'required',
        'provis_id' => 'required',
        'dns' => 'required',
        'port' => 'required',
        'suffix' => 'required',
        'type' => 'required',
        // 'origin' => 'required',
        'bussines_name' => 'required',
        'cif' => 'required',
        'address' => 'required',
        'postal_code' => 'required',
        'city_id' => 'required',
        'providence_id' => 'required'
    ];


    public static $messages = [
        'name.required' => 'El nombre del club es obligatorio.',
        'pv_pk.required' => 'La llave publica es obligatoria.',
        'pv_sk.required' => 'La llave privada es obligatoria.',
        'uuid.required' => 'El id de Altafit es obligatoria.',
        'provis_id.required' => 'El id es obligatorio.',
        'dns.required' => 'DNS es obligatoria.',
        'port.required' => 'El puerto es obligatorio.',
        'suffix.required' => 'El sufijo es obligatorio.',
        'type.required' => 'El tipo es obligatorio.',
        // 'origin.required' => 'El tipo es obligatorio.',
        'bussines_name.required' => 'La razón social es obligatoria.',
        'cif.required' => 'El CIF es obligatorio.',
        'address.required' => 'La dirección es obligatoria.',
        'postal_code.required' => 'El código postal es obligatorio.',
        'city_id.required' => 'La ciudad es obligatoria.',
        'providence_id.required' => 'La providencia es obligatoria.'
    ];


    public function scopeCity($query,$param){
        return $query->where('city_id','=',$param);
    }

    public function scopeProvidence($query,$param){
        return $query->where('providence_id','=',$param);
    }

    public function scopeDirector($query,$param){
        return $query->where('director_id','=',$param);
    }

    // public function scopeCoordinator($query,$param){
    //     return $query->where('coordinator_id','=',$param);
    // }

    public function scopeParams($query,$param){
        $searchParams = '(clubs.name LIKE ? OR clubs.uuid LIKE ? OR clubs.bussines_name LIKE ? OR clubs.type LIKE ? OR clubs.cif LIKE ?)';
        $query->whereRaw($searchParams, array("%$param%", "%$param%", "%$param%", "%$param%", "%$param%"));
    }

    public function getCityAttribute(){
        $var = isset($this->attributes['city_id']) ? $this->attributes['city_id'] : ''; 
        return City::find($var) ? City::find($this->attributes['city_id'])->name : '';
    }

    public function getProvidenceAttribute(){
        $var = isset($this->attributes['providence_id']) ? $this->attributes['providence_id'] : '';
        return Providence::find($var) ? Providence::find($this->attributes['providence_id'])->name : '';
    }

    public function getDirectorAttribute(){
        $var = isset($this->attributes['director_id']) ? $this->attributes['director_id'] : ''; 
        if($user = User::find($var)){
            return $user->name.' '.$user->profile->lastname;
        }else{
            return '';
        }

    }

    // public function getCoordinatorAttribute(){
    //     $var = isset($this->attributes['coordinator_id']) ? $this->attributes['coordinator_id'] : ''; 
    //     if($user = User::find($var)){
    //         return $user->name.' '.$user->profile->lastname;
    //     }else{
    //         return '';
    //     }
    // }

}
