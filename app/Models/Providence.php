<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Providence extends Model
{
    use SoftDeletes;

    public $table = 'providences';
    

    protected $dates = ['deleted_at'];


    public $fillable = [
        'name',
    ];


    public function setNameAttribute($value){
    	$this->attributes['name'] = ucfirst($value);
    }
    
}
