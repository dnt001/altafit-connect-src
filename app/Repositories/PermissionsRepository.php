<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class PermissionsRepository
 * @package App\Repositories
 * @version March 9, 2018, 3:59 pm UTC
 *
 * @method Permissions findWithoutFail($id, $columns = ['*'])
 * @method Permissions find($id, $columns = ['*'])
 * @method Permissions first($columns = ['*'])
*/
class PermissionsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Permission::class;
    }
}
