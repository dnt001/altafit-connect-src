<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role;
use App\Notifications\CreateUser;

use App\User;
use App\Models\Profile;

use Hash;
use DB;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version July 30, 2018, 9:15 pm UTC
 *
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class UserRepository 
{

    public function all(){
      return User::all();
    }


    public function findWithoutFail($id){
        if($user = User::find($id)){
            $user->roles = isset($user->roles[0]) ? $user->roles[0]->name: ''; 

            if($profile = Profile::where('user_id','=',$id)->first()){
                $user->lastname = $profile->lastname; 
                $user->phone = $profile->phone; 
            }
        }else{
            $user = [];
        }
        

        return $user;
    }

    public function delete($id){
        return User::find($id) ? User::find($id)->delete() : [];
    }


    public function getList($search){
        $users = User::select('users.id','users.name','users.email','roles.name as roles')->leftJoin('model_has_roles','model_has_roles.model_id','=','users.id')->leftJoin('roles','model_has_roles.role_id','=','roles.id')->withTrashed();

        if($search != ''){
            $users = $users->params($search);
        } 

        $users = $users->paginate(50);
        return $users;
    }

    public function getRoles(){
        $role = new Role();
        $listRoles = $role->pluck('name', 'name')->prepend('-Selecciona una opción-','')->all();
        return $listRoles;
    }


    public function createSystemUser($data){
        $existMail = User::where('email', '=', $data['email'])->get();
        
        if(!$existMail->isEmpty()){
            return false;
        }

        $user = new User();
        $user->name         = $data['name'];
        $user->email        = $data['email']; 
        $user->password     = $data['password'];
        $user->save();    

        $user->notify(new CreateUser($data));

        $profile = new Profile();
        $profile->lastname      = $data['lastname'];
        $profile->phone         = $data['phone'];
        $profile->user_id       = $user->id;
        $profile->save();


        $user->assignRole($data['roles']);

        return true;

    }

    public function updateSystemUser($data, $id){
        $user = User::find($id);
        $user->name         = $data['name']; 

        if($user->email != $data['email']){
            if(!$existMail = User::where('email', '=', $data['email'])->first()){
                $user->email = $data['email'];
            }else{
                return false;
            }
        }

        $user->save();

        $profile = Profile::where('user_id','=',$id)->first();
        $profile->lastname      = $data['lastname'];
        $profile->phone         = $data['phone'];
        $profile->user_id       = $user->id;
        $profile->save();

        $user->syncRoles($data['roles']);

        return true;
    }


}
