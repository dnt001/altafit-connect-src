<?php

namespace App\Repositories;

use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

use InfyOm\Generator\Common\BaseRepository;

/**
 * Class GroupsRepository
 * @package App\Repositories
 * @version March 8, 2018, 6:28 pm UTC
 *
 * @method Groups findWithoutFail($id, $columns = ['*'])
 * @method Groups find($id, $columns = ['*'])
 * @method Groups first($columns = ['*'])
*/
class GroupsRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Role::class;
    }
  
    public function getListPermissions($permissionsList){
    
      $permissions = new Permission();
      $permissionCollection = [];
      
      if( $permissionsList->isEmpty()){
        $list = $permissions->orderBy('id','ASC')->get();
      }else{
        $list = $permissionsList;
      }
      
      foreach($list as $key => $item){
        $elementPermissions = explode('_',$item->name);
        if(isset($elementPermissions[1])){
            $permissionCollection[$elementPermissions[1]][] =  implode('_',$elementPermissions);
        }else{
            $permissionCollection[$elementPermissions[0]][]= implode('_',$elementPermissions);
        }
      }

      return $permissionCollection;
    }
  
  
    public function createGroupAndSyncPermissions($data){
      try{
          $groups = new Role();
          $groups->name = $data['name'];
          $groups->save();

          if(isset($data['permissions'])){
            $groups->syncPermissions($data['permissions']);
          }

          return true;
      }catch(Exception $e){
        report($e);
        return false;
      }
    }
  
    public function updateGroupAndSyncPermissions($data,$id){
      try{
        $groups = Role::find($id);
        $listPermissionsGive = $groups->permissions;
        
        foreach($listPermissionsGive as $value){
            $groups->revokePermissionTo($value->name);
        }

        $groups->name = $data['name'];
        $groups->save();
        
        if(isset($data['permissions'])){
          $groups->syncPermissions($data['permissions']);
        }

        return true;
      }catch(Exception $e){
        report($e);
        return false;
      }
    }
    
}
