<?php

namespace App\Repositories;

use App\Models\Club;
use InfyOm\Generator\Common\BaseRepository;

/**
 * Class ClubRepository
 * @package App\Repositories
 * @version May 1, 2019, 4:57 pm UTC
 *
 * @method Club findWithoutFail($id, $columns = ['*'])
 * @method Club find($id, $columns = ['*'])
 * @method Club first($columns = ['*'])
*/
class ClubRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'name',
        'pv_sk',
        'pv_pk',
        'uuid',
        'provis_id',
        'dns',
        'port',
        'suffix',
        'type',
        'origin',
        'bussines_name',
        'cif',
        'address',
        'postal_code',
        'city',
        'providence',
        'director_id',
        'coordinator'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Club::class;
    }

    public function getListClubs($request){
        $club = Club::query();

        if($request->city_id && !$request->providence_id && !$request->director_id  && !$request->search ){
            $club->city($request->city_id);
        }elseif($request->providence_id &&  !$request->city_id && !$request->director_id   && !$request->search ){
            $club->providence($request->providence_id);
        }elseif($request->director_id && !$request->city_id && !$request->providence_id  && !$request->search ){
            $club->director($request->director_id);
        }elseif(!$request->coordinator_id && !$request->city_id && !$request->providence_id && !$request->director_id  && $request->search ){
            $club->params($request->search);
        }elseif($request->city_id && $request->providence_id && !$request->director_id  && !$request->search ){
            $club->city($request->city_id)->providence($request->providence_id);
        }elseif($request->city_id && !$request->providence_id && $request->director_id  && !$request->search ){
            $club->city($request->city_id)->director($request->director_id);
        }elseif($request->city_id && !$request->providence_id && !$request->director_id  && !$request->search ){
            $club->city($request->city_id);
        }elseif(!$request->city_id && $request->providence_id && $request->director_id  && !$request->search ){
            $club->providence($request->providence_id)->director($request->director_id);
        }elseif(!$request->city_id && $request->providence_id && !$request->director_id  && !$request->search ){
            $club->providence($request->providence_id);
        }elseif(!$request->city_id && !$request->providence_id && $request->director_id  && !$request->search ){
            $club->director($request->director_id);
        }



        return $club->orderBy('uuid','ASC')->paginate(15);
    }
}
