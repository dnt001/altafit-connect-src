<?php

namespace App\Repositories;

use Spatie\Permission\Models\Role;
use App\User;

use App\Models\City;
use App\Models\Providence;


use Hash;
use DB;

/**
 * Class UserRepository
 * @package App\Repositories
 * @version July 30, 2018, 9:15 pm UTC
 *
 * @method User findWithoutFail($id, $columns = ['*'])
 * @method User find($id, $columns = ['*'])
 * @method User first($columns = ['*'])
*/
class QueryRepository 
{
	public function getCities(){
		return City::get();
	}
	
	public function getProvidences(){
		return Providence::get();
	}


	public function getDirectors(){
		return User::role('Director Club')->get();
	}
}
