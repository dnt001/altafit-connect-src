<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Auth;
use Illuminate\Database\Eloquent\SoftDeletes;

use App\Models\Profile;
use App\Models\Club;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','company_id'
    ];

    protected $appends = ['profile'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];


    public static $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users',
        'roles' => 'required',
        'lastname' => 'required',
        'phone' => 'required'
    ];

    public static $rulesUpdate = [
        'name' => 'required',
        'email' => 'required|email',
        'roles' => 'required',
        'lastname' => 'required',
        'phone' => 'required'

    ];

    public function getProfileAttribute(){
        return Profile::where('user_id','=',$this->attributes['id'])->first();
    }

    public function scopeParams($query,$param){
        $searchParams = '(users.name LIKE ? OR users.email LIKE ? OR roles.name LIKE ? )';
        $query->whereRaw($searchParams, array("%$param%", "%$param%", "%$param%"));
    }

    public function getClubs(){
        if(Auth::user()->roles[0]->name == 'SuperAdministrador'){
            return Club::limit(10)->pluck('name','id')->all();
        }else{
            return Club::where('director_id','=',Auth::user()->id)->pluck('name','id')->all();
        }
    }
    
}
