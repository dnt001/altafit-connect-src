<?php 

  function translate_keywords($keyword){
    if($keyword == 'view'){
      return 'Ver';
    }else if($keyword == 'create'){
      return 'Crear';
    }else if($keyword == 'edit'){
      return 'Editar';
    }else if($keyword == 'delete'){
      return 'Eliminar';
    }else{
      return 'Módulo '.$keyword;
    }
  }


  function remove_keyword($keyword){
    $key = explode('_',$keyword);
    array_pop($key);
    
    return implode('_',$key);
    
  }