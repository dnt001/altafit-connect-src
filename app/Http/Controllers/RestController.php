<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Repositories\QueryRepository;
use App\Http\Requests\CreateCityRequest;
use App\Http\Requests\CreateProvidenceRequest;

use App\Notifications\CreateUser;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Providence;
use App\Models\City;
use App\User;

use Exception;
use Response;
use Flash;
use Excel;
use Auth;
use Mail;
use URL;


class RestController extends Controller
{
    private $queryRepository;

    public function __construct(QueryRepository $queryRepo)
    {
        $this->middleware('auth');
        $this->queryRepository = $queryRepo;

    }

    public function active($id){
        $password = Str::random(10);

        $user = User::withTrashed()->find($id);
        $user->deleted_at = null;
        $user->password = bcrypt($password);
        $user->save();

        $data = [
            'name'      => $user->name,
            'lastname'  => $user->profile->lastname,
            'email'     => $user->email,
            'send'      => $password
        ];  

        $user->notify(new CreateUser($data));

        Flash::success('Usuario activado correctamente');
        return redirect(route('users.index'));
    }

    public function selected($club){
        $user = Auth::user();
        $user->club_id = $club;
        $user->save();
        Flash::success('Club seleccionado correctamente.');

        return redirect()->back();
    }

    public function unactive($id){
        $user = User::find($id);
        $user->active = 0;
        $user->save();

        Flash::success('Usuario desactivado correctamente');
        return redirect(route('users.index'));
    }



    public function  saveCities(Request $request){
        $input = [
            'name'      => $request->input('name','')
        ];

        $cities = City::pluck('id','name')->all();

        if($input['name'] == ''){
            return response()->json([
                'message' => 'La ciudad no puede ir vacía.',
                'status' => 500
            ]); 
        }

        

        if(isset($cities[ucfirst($input['name'])])){
            return response()->json([
                'message' => 'La ciudad ya existe, no puedes registrar de nueva cuenta la ciudad.',
                'status' => 500
            ]); 
        }

        if($request->ajax()){    
            City::create($input);

            return response()->json([
                'message' => 'Ciudad creada correctamente.',
                'status' => 200
            ]); 
        }
    }  

    public function  saveProvidences(Request $request){
        $input = [
            'name'      => $request->input('name','')
        ];

        $providences = Providence::pluck('id','name')->all();

        if($input['name'] == ''){
            return response()->json([
                'message' => 'La provincia no puede ir vacía.',
                'status' => 500
            ]); 
        }

        if(isset($providences[ucfirst($input['name'])])){
            return response()->json([
                'message' => 'La provincia ya existe, no puedes registrar de nueva cuenta la provincia.',
                'status' => 500
            ]); 
        }

        if($request->ajax()){    
            Providence::create($input);

            return response()->json([
                'message' => 'Provincia creada correctamente.',
                'status' => 200
            ]); 
        }
    }  

    public function cities(Request $request){
        if($request->ajax()){
            $cities = $this->queryRepository->getCities();
            return response()->json([
                'data' => $cities
            ]); 
        }
    }  

    public function providences(Request $request){
        if($request->ajax()){
            $providences = $this->queryRepository->getProvidences();
            return response()->json([
                'data' => $providences
            ]); 
        }
    } 

    public function directors(Request $request){
        if($request->ajax()){
            $directors = $this->queryRepository->getDirectors();
            return response()->json([
                'data' => $directors
            ]); 
        }
    }

    // public function coordinators(Request $request){
    //     if($request->ajax()){
    //         $coordinators = $this->queryRepository->getCoordinators();
    //         return response()->json([
    //             'data' => $coordinators
    //         ]); 
    //     }
    // }

   
}
