<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

use Validator;
use Response;
use Flash;
use Auth;

class UserController extends Controller
{
    /** @var  UserDataRepository */
    private $userDataRepository;

    public $user;

    public function __construct(UserRepository $userDataRepo)
    {
        $this->middleware('auth');

        $this->userDataRepository = $userDataRepo;
    }


    public function changePassword(Request $request){

        $customMessages = [
            'required' => 'La contraseña es obligatoria.',
            'min' => 'La contraseña debe tener minimo 8 caracteres.',
            'regex' => 'La contraseña debe contener al menos un caracter especial, una mayúscula, letras, números y ser mínimo de 8 caracteres.'
        ];

        if($request->input('password') != $request->input('repeat_password')){
            Flash::error('Las contraseñas no coinciden.');

            return redirect()->back();
        }


        $validator = Validator::make($request->all(), [
            'password' => 'required|min:8|regex:/^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/',
        ],$customMessages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            $list = $errors->getMessages();
            $concat = '';

            foreach($list['password'] as $l){
                $concat .= $l.PHP_EOL;
            }

            Flash::error($concat);

            return redirect()->back();
        }

        $user = Auth::user();
        $user->password = bcrypt($request->input('password'));

        if(Auth::user()->first_login == 0){
            $user->first_login = 1;
        }

        $user->save();

        if($user->save()){
            Flash::success('Las contraseña ha sido modificada correctamente.');
            return redirect()->back();  
        }else{
            Flash::error('Ha ocurrido un error intentalo más tarde.');

            return redirect()->back();
        }

        
    }

    /**
     * Display a listing of the UserData.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {

        if(!Auth::user()->hasPermissionTo('view_users')){
            abort(404);
        }
        $search = $request->input('search','');
        $users = $this->userDataRepository->getList($search); 
        return view('users.index')
            ->with('users', $users)
            ->with('search',$search);
    }


    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        if(!Auth::user()->hasPermissionTo('create_users')){
            abort(404);
        }

        $roles = $this->userDataRepository->getRoles();
        return view('users.create')->with('roles',$roles);
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $password = Str::random(10);

        $dataUser = [
            'name'          => $request->input('name', ''),
            'email'         => $request->input('email', ''),
            'roles'         => $request->input('roles', ''),
            'password'      => bcrypt($password),
            'send'          => $password,
            'lastname'      => $request->input('lastname', ''),
            'phone'         => $request->input('phone','')
        ];
        
        $user = $this->userDataRepository->createSystemUser($dataUser);

        if($user){
            Flash::success('Usuario creado correctamente');
        }else{
            Flash::error('El correo ya existe intenta con otro correo.');
        }

        return redirect(route('users.index'));
    }


    public function edit($id)
    {
        if(!Auth::user()->hasPermissionTo('edit_users')){
            abort(404);
        }

        $users = $this->userDataRepository->findWithoutFail($id);
        $roles = $this->userDataRepository->getRoles();

        if (empty($users)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }
        return view('users.edit')->with('user', $users)->with('roles',$roles);
    }


    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userDataRepository->findWithoutFail($id);

        $data = [
            'name'          => $request->input('name', ''),
            'email'         => $request->input('email', ''),
            'roles'         => $request->input('roles', ''),
            'lastname'      => $request->input('lastname', ''),
            'phone'         => $request->input('phone','')
        ];


        if (empty($user)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }

        $user = $this->userDataRepository->updateSystemUser($data, $id);

        if($user){
            Flash::success('Usuario modificado correctamente');
        }else{
            Flash::error('El correo ya existe intenta con otro correo.');
        }

        return redirect(route('users.index'));
    }


    /**
     * Display the specified UserData.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $userData = $this->userDataRepository->findWithoutFail($id);
        

        if (empty($userData)) {
            Flash::error('User Data not found');

            return redirect(route('userDatas.index'));
        }

        return view('users.show')->with('userData', $userData);
    }


    public function destroy($id)
    {
        if(!Auth::user()->hasPermissionTo('delete_users')){
            abort(404);
        }

        $users = $this->userDataRepository->findWithoutFail($id);

        if (empty($users)) {
            Flash::error('Usuario no encontrado');

            return redirect(route('users.index'));
        }

        $this->userDataRepository->delete($id);

        Flash::success('Usuario desactivado correctamente.');

        return redirect(route('users.index'));
    }


}
