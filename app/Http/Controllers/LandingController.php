<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth, Validator;
use Illuminate\Http\Request;
use Session;
use DB;

use Illuminate\Support\Str;
use Carbon\Carbon;

use App\Notifications\ResetPassword;
use App\Notifications\Recovery;

class LandingController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */


    public function recovery($token){
        if($exist = DB::table('password_resets')->where('token','=',$token)->first()){
            if($user = User::where('email', '=', $exist->email)->first()){
                $password = Str::random(10);
                
                $user->first_login = 0;
                $user->password = bcrypt($password);
                $user->save();
                
                $data = [
                    'name'      => $user->name,
                    'lastname'  => $user->profile->lastname,
                    'email'     => $user->email,
                    'send'      => $password
                ];  


                $user->notify(new Recovery($data));


                $exist = DB::table('password_resets')->where('token','=',$token)->delete();

                return view('reset');
            }else{
                abort(404);
            }
        }else{
            abort(404);
        }
    }

    public function login(Request $request)
    {
        $group = $request->input('email');
        $passd = $request->input('password');
        
        if (Auth::attempt(['email' => $group, 'password' => $passd])) {
            $user = Auth::user();
            $redirect = '/home';
            return redirect($redirect);
        }else{
            if($user = User::where('email','=',$group)->withTrashed()->first()){
                if(!is_null($user->deleted_at)){
                    return view("auth.login")->with('errorsLogin', "El usuario ha sido dado de baja, contacta con soporte.");
                } else{
                    return view("auth.login")->with('errorsLogin', "Correo electrónico o contraseña incorrecta. Por favor verifica tus datos.");
                }
            }
        }
    }

    public function logout(Request $request){
        Auth::logout();
        return redirect('/login');
    }

    public function reset(Request $request){
         if($user = User::where('email', '=', $request->email)->first()){
            $token = Str::random(60);
            DB::table('password_resets')->insert([
                'email' => $request->email,
                'token' => $token, //change 60 to any length you want
                'created_at' => Carbon::now()
            ]);

            $url = route('recovery',$token);
            $user->notify(new ResetPassword($url,$user));
            
            session(['errors' => 'Te hemos enviado por email un enlace para cambiar tu contraseña.','status' => 200]);
            return view("auth.passwords.email");
        }else{
            session(['errors' => 'El usuario solicitado, no existe.','status' => 401]);
            return view("auth.passwords.email");
        }
    }
}
