<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateGroupsRequest;
use App\Http\Requests\UpdateGroupsRequest;
use App\Repositories\GroupsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class GroupsController extends AppBaseController
{
    /** @var  GroupsRepository */
    private $groupsRepository;

    public function __construct(GroupsRepository $groupsRepo)
    {
        $this->groupsRepository = $groupsRepo;
    }

    /**
     * Display a listing of the Groups.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->groupsRepository->pushCriteria(new RequestCriteria($request));
        $groups = $this->groupsRepository->all();

        return view('groups.index')
            ->with('groups', $groups);
    }

    /**
     * Show the form for creating a new Groups.
     *
     * @return Response
     */
    public function create()
    {
        $listPermissions = $this->groupsRepository->getListPermissions(collect([]));
      
        return view('groups.create')->with('list',$listPermissions);
    }

    /**
     * Store a newly created Groups in storage.
     *
     * @param CreateGroupsRequest $request
     *
     * @return Response
     */
    public function store(CreateGroupsRequest $request)
    {
        $input = $request->all();
        $groups = $this->groupsRepository->createGroupAndSyncPermissions($input);
      
        if($groups){
          Flash::success('Grupo creado correctamente.');
        }else{
          Flash::error('Ocurrio un error contacta al administrador.');
        }
        
        return redirect(route('groups.index'));
    }

    /**
     * Display the specified Groups.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $groups = $this->groupsRepository->findWithoutFail($id);
        $listPermissions = $this->groupsRepository->getListPermissions($groups->permissions);
        
        if (empty($groups)) {
            Flash::error('Grupo no encontrado');

            return redirect(route('groups.index'));
        }

        return view('groups.show')->with('groups', $groups)->with('list',$listPermissions);
    }

    /**
     * Show the form for editing the specified Groups.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $groups = $this->groupsRepository->findWithoutFail($id);
        $listPermissions = $this->groupsRepository->getListPermissions(collect([]));
      
        if (empty($groups)) {
            Flash::error('Grupo no encontrado');
            return redirect(route('groups.index'));
        }

        
      
        return view('groups.edit')->with('groups', $groups)->with('list',$listPermissions);
    }

    /**
     * Update the specified Groups in storage.
     *
     * @param  int              $id
     * @param UpdateGroupsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateGroupsRequest $request)
    {
        $groups = $this->groupsRepository->findWithoutFail($id);

        if (empty($groups)) {
            Flash::error('Grupo no encontrado');

            return redirect(route('groups.index'));
        }

        $groups = $this->groupsRepository->updateGroupAndSyncPermissions($request->all(), $id);
  
        if($groups){
          Flash::success('Grupo editado correctamente.');
        }else{
          Flash::error('Ocurrio un error contacta al administrador.');
        }
        
        return redirect(route('groups.index'));
    }

    /**
     * Remove the specified Groups from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $groups = $this->groupsRepository->findWithoutFail($id);

        if (empty($groups)) {
           Flash::error('Grupo no encontrado');
            return redirect(route('groups.index'));
        }

        $this->groupsRepository->delete($id);

        Flash::success('Grupo eliminado correctamente.');

        return redirect(route('groups.index'));
    }
}
