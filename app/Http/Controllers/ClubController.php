<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateClubRequest;
use App\Http\Requests\UpdateClubRequest;
use App\Repositories\ClubRepository;
use App\Repositories\QueryRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use App\Imports\ClubImport;
use App\Exports\ClubExport;
use App\Models\Club;

use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
use Exception;
use Excel;

class ClubController extends AppBaseController
{
    /** @var  ClubRepository */
    private $clubRepository;
    private $queryRepository;

    public function __construct(ClubRepository $clubRepo, QueryRepository $queryRepo)
    {
        $this->middleware('auth');
        $this->clubRepository = $clubRepo;
        $this->queryRepository = $queryRepo;
    }

    public function active($id){
        $club = Club::find($id);
        $club->active = 1;
        $club->save();

        Flash::success('Club activado correctamente');
        return redirect(route('clubs.index'));
    }

    public function unactive($id){
        $club = Club::find($id);
        $club->active = 0;
        $club->save();

        Flash::success('Club desactivado correctamente');
        return redirect(route('clubs.index'));
    }

    public function imports(){
        return view('imports.index');
    }

    public function upload(Request $request){
        Excel::import(new ClubImport, $request->file('file'));
        Flash::success('Archivo cargado correctamente.');
        return view('imports.index');
    }

    public function export(Request $request){
        return Excel::download(new ClubExport, 'clubs-export.xlsx');
    }

    /**
     * Display a listing of the Club.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->clubRepository->pushCriteria(new RequestCriteria($request));
        $clubs = $this->clubRepository->getListClubs($request);

        $cities = $this->queryRepository->getCities();
        $providences = $this->queryRepository->getProvidences();
        $directors = $this->queryRepository->getDirectors();
        // $coordinators = $this->queryRepository->getCoordinators();

        return view('clubs.index')
            ->with('clubs', $clubs)
            ->with('cities', $cities)
            ->with('providences', $providences)
            ->with('directors', $directors)
            // ->with('coordinators', $coordinators)
            ->with('city', $request->input('city_id',''))
            ->with('providence', $request->input('providence_id',''))
            ->with('director', $request->input('director_id',''))
            ->with('coordinator', $request->input('coordinator_id',''))
            ->with('search', $request->input('search',''))
            ->with('view', $request->input('view','card'));
    }

    /**
     * Show the form for creating a new Club.
     *
     * @return Response
     */
    public function create()
    {
        return view('clubs.create');
    }

    /**
     * Store a newly created Club in storage.
     *
     * @param CreateClubRequest $request
     *
     * @return Response
     */
    // public function store(CreateClubRequest $request)
    // {
    //     $input = $request->all();

    //     $club = $this->clubRepository->create($input);

    //     Flash::success('Club saved successfully.');

    //     return redirect(route('clubs.index'));
    // }

    public function storeClub(CreateClubRequest $request)
    {
        try {
            if($request->ajax()){
                $input = $request->all();
                $club = $this->clubRepository->create($input);

                if($club){
                    return response()->json([
                        'status'  => 200,
                        'message' => 'Se ha creado correctamente el club.'
                    ]); 
                }else{
                    return response()->json([
                        'status'  => 500,
                        'message' => 'Ha ocurrido un error intentelo más tarde.'
                    ]); 
                }
            }
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
            return response()->json([
                'status'  => 500,
                'message' => $errors
            ]);        
        }
    }

    /**
     * Display the specified Club.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $club = $this->clubRepository->findWithoutFail($id);

        if (empty($club)) {
            Flash::error('Club not found');

            return redirect(route('clubs.index'));
        }

        return view('clubs.show')->with('club', $club);
    }

    /**
     * Show the form for editing the specified Club.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $club = $this->clubRepository->findWithoutFail($id);

        if (empty($club)) {
            Flash::error('Club not found');

            return redirect(route('clubs.index'));
        }

        return view('clubs.edit')->with('club', $club);
    }

    /**
     * Update the specified Club in storage.
     *
     * @param  int              $id
     * @param UpdateClubRequest $request
     *
     * @return Response
     */

    public function updateClub($id, UpdateClubRequest $request)
    {

        try {
            if($request->ajax()){
                $club = $this->clubRepository->findWithoutFail($id);

                if (empty($club)) {
                    return response()->json([
                        'status'  => 500,
                        'message' => 'El club solicitado no ha sido encontrado.'
                    ]); 
                }

                $club = $this->clubRepository->update($request->all(), $id);

                if($club){
                    return response()->json([
                        'status'  => 200,
                        'message' => 'Se ha modificado correctamente el club.'
                    ]); 
                }else{
                    return response()->json([
                        'status'  => 500,
                        'message' => 'Ha ocurrido un error intentelo más tarde.'
                    ]); 
                }
            }
        } catch (Exception $e) {
            $errors[] = $e->getMessage();
            return response()->json([
                'status'  => 500,
                'message' => $errors
            ]);        
        }

    }

    // public function update($id, UpdateClubRequest $request)
    // {
    //     $club = $this->clubRepository->findWithoutFail($id);

    //     if (empty($club)) {
    //         Flash::error('Club not found');

    //         return redirect(route('clubs.index'));
    //     }

    //     $club = $this->clubRepository->update($request->all(), $id);

    //     Flash::success('Club updated successfully.');

    //     return redirect(route('clubs.index'));
    // }

    /**
     * Remove the specified Club from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    // public function destroy($id)
    // {
    //     $club = $this->clubRepository->findWithoutFail($id);

    //     if (empty($club)) {
    //         Flash::error('Club not found');

    //         return redirect(route('clubs.index'));
    //     }

    //     $this->clubRepository->delete($id);

    //     Flash::success('Club deleted successfully.');

    //     return redirect(route('clubs.index'));
    // }


    public function deleteClub(Request $request)
    {
        $club = $this->clubRepository->findWithoutFail($request->input('id'));

        if (empty($club)) {
            Flash::error('Club not found');

            return redirect(route('clubs.index'));
        }

        $this->clubRepository->delete($request->input('id'));

        Flash::success('Club eliminado correctamente.');

        return redirect(route('clubs.index'));
    }
}
