<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\Owner;

class CreateCityRequest extends FormRequest
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $data['name'] = 'required';
        return $data;
    }

    public function messages(){
        $data['name.required'] = 'El nombre de la ciudad es obligatorio.';
        return $data;
    }
}
