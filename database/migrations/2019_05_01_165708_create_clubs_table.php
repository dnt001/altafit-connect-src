<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateClubsTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('providences', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });


        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            
            $table->string('name');

            $table->text('pv_sk');
            $table->text('pv_pk');
            $table->string('provis_id');
            $table->string('dns');
            $table->string('port');

            $table->text('uuid');
            
            $table->string('suffix');
            $table->string('type');
            $table->string('origin');
            $table->string('bussines_name');
            $table->string('cif');
            $table->string('address');
            $table->string('postal_code');

            $table->integer('city_id')->unsigned()->index()->nullable();

            $table->foreign('city_id')
                    ->references('id')->on('cities')
                    ->onDelete('cascade')->onUpdate('cascade');

            $table->integer('providence_id')->unsigned()->index()->nullable();

            $table->foreign('providence_id')
                    ->references('id')->on('providences')
                    ->onDelete('cascade')->onUpdate('cascade');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clubs');
    }
}
