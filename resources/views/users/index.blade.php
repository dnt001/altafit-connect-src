@extends('layouts.app')

@section('header')
    <section class="content-header">
        <h1 class="right">
           <a class="btn btn-primary btn-header search-filters" 
                 data-toggle="tooltip" data-placement="bottom" title="Abrir filtros de busqueda" data-original-title="Abrir filtros de busqueda"
                href="#">
               <i class="mdi mdi-filter"></i>
           </a>
        </h1>
    </section>
@endsection

@section('content')
   <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash::message')
                <div class="card search-filters-panel">
                    <div class="card-body">
                        <form action="/users" method="GET">
                            
                            <div class="col-sm-12 form-group element-input pd-zero">
                                <label for="">Busqueda</label>
                                <input type="text" value="" class="form-control" name="search">
                            </div>

                            <div class="col-sm-12 form-group element-input pd-zero">
                                <button class="right btn btn-primary">Buscar</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="card">
                    <div class="card-header">
                        <h3>
                            {{ __('Listado de usuarios') }}
                            @can('create_users')
                                <a 
                                    data-toggle="tooltip" title="Crear usuarios"
                                    data-placement= 'top'
                                    class="btn btn-primary right single-button btn-xs"  href="{!! route('users.create')  !!}">
                                    <div class="icon-basf" style="background-image: url({{ asset('img/add-user.png') }})"></div>
                                </a>
                            @endcan
                        </h3>
                    </div>

                    <div class="card-body">
                        @include('users.table')
                    </div>

                    <div class="card-footer">
                        <div class="text-center">
                            {{ $users->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
