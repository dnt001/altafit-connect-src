<div class="table-responsive">
    <table class="table" id="userDatas-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th>Correo electrónico</th>
                <th>Rol</th>
                <th class="text-center" colspan="3">Acciones</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $userData)
                <tr>
                    <td>{!! $userData->name !!}</td>
                    <td>{!! $userData->email !!}</td>
                    <td>{!! $userData->roles !!}</td>
                    <td>
                        {!! Form::open(['route' => ['users.destroy', $userData->id], 'method' => 'delete' , 'class' => 'form-buttons']) !!}
                            <div class='btn-group'>
                                @can('edit_users')
                                    <a href="{!! route('users.edit', [$userData->id]) !!}" 
                                        data-toggle="tooltip" title="Editar usuario"
                                        data-placement= 'top'
                                        class='btn btn-info single-button btn-xs'>
                                        <div class="icon-basf-small" style="background-image: url({{ asset('img/edit.png') }})"></div>
                                    </a>
                                @endcan
                                @can('delete_users')
                                    {{-- @if($userData->active == 1)
                                        <a 
                                            href="{{ route('unactive',$userData->id) }}"
                                            data-toggle="tooltip" title="Activar usuario"
                                            data-placement= 'top'
                                             class="btn btn-warning single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/ban.png') }})"></div>
                                        </a>
                                    @endif

                                    @if($userData->active == 0)
                                        <a 
                                            href="{{ route('active',$userData->id) }}" 
                                            data-toggle="tooltip" title="Desactivar usuario"
                                            data-placement= 'top'
                                             class="btn btn-success single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/success.png') }})"></div>
                                        </a>
                                        
                                    @endif --}}
                                    
                                    @if(is_null($userData->deleted_at))
                                        <button 
                                            data-toggle="tooltip" title="Borrar usuario"
                                            data-placement= 'top'
                                            onclick="return confirm('Estas seguro de desactivar este recurso?')" type="submit" class="btn btn-warning single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/ban.png') }})"></div>
                                        </button>
                                    @else
                                        <a 
                                            href="{{ route('active',$userData->id) }}" 
                                            data-toggle="tooltip" title="Desactivar usuario"
                                            data-placement= 'top'
                                             class="btn btn-success single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/success.png') }})"></div>
                                        </a>
                                    @endif
                                @endcan
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
        @endforeach
        </tbody>
    </table>
</div>
