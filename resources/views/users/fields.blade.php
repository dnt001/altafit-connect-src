<div class="col-sm-6 form-group element-input">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-6 form-group element-input">
    {!! Form::label('lastname', 'Apellido:') !!}
    {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-6 form-group element-input">
    {!! Form::label('email', 'Correo electrónico:') !!}
    {!! Form::email('email', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-6 form-group element-input">
    {!! Form::label('phone', 'Teléfono:') !!}
    {!! Form::text('phone', null, ['class' => 'form-control']) !!}
</div>

<div class="col-sm-12 form-group element-input">
    {!! Form::label('roles', '  Roles:') !!}
    {!! Form::select('roles', $roles, null, ['class' => 'form-control roles']) !!}
</div>



{{-- <div class="form-group col-sm-2 form-group element-input">
    {!! Form::label('age', 'Edad:') !!}
    {!! Form::number('age', null, ['class' => 'form-control']) !!}
</div>

<!-- Gender Field -->
<div class="form-group col-sm-2 form-group element-input">
    {!! Form::label('gender', 'Genero:') !!}
    {!! Form::select('gender', ['1' => 'Masculino', '2' => 'Femenino'], null, ['class' => 'form-control']) !!}
</div>

<!-- Birthdate Field -->
<div class="form-group col-sm-4 form-group element-input">
    {!! Form::label('birthdate', 'Fecha de nacimiento:') !!}
    {!! Form::text('birthdate', null, ['class' => 'form-control date']) !!}
</div> --}}


<!-- Submit Field -->
<div class="col-sm-12 pd-zero">
    {!! Form::submit('Guardar', ['class' => 'btn btn-primary single-button right']) !!}
    <a href="{!! route('users.index') !!}" class="btn btn-default single-button right">Cancelar</a>
</div>


