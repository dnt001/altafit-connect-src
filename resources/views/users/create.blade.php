@extends('layouts.app')

@section('content')
   <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h3>
                            {{ __('Crear usuarios') }}
                        </h3>
                    </div>

                    <div class="card-body">

                        @if ($errors->any())
                            <div class="alert alert-warning alert-dismissible">
                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                                <h4><i class="icon fa fa-warning"></i> Cuidado!</h4>
                                Debe verificar los siguientes errores antes de continuar:
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif

                        
                        {!! Form::open(['route' => ['users.store'], 'method' => 'post','files' => true]) !!}

                            @include('users.fields')

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
