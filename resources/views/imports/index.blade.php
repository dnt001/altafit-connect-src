@extends('layouts.app')

@section('header')
    <section class="content-header">
        <h1 class="left">
            <i class="mdi mdi-file-import"></i>
            Importar clubs
        </h1>
        <h1 class="right">
           <a class="btn btn-primary btn-header" 
                data-toggle="tooltip" data-placement="bottom" data-original-title="Regresar al listado"
                href="{!! route('clubs.index') !!}">
               <i class="mdi mdi-keyboard-backspace"></i>
           </a>
        </h1>
    </section>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 pd-zero">
                @include('flash::message')
                <div class="card shadow mb-4">
                    <div class="card-body">
                        {!! Form::open(['route' => 'imports.upload', 'files'=> true]) !!}
                            <div class="form-group mt-4">
                                <label for="">Archivo importar:</label>
                                <input type="file" name="file" class="form-control">
                            </div>

                            <button class="btn btn-primary btn-md right" type="submit">Cargar</button>

                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    
@endsection