<div class="card">
    <div class="card-body card-table-club">
        <div class="table-responsive">
            <table class="table" id="clubs-table">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nombre</th>
                        <th>Origen</th>
                        <th>Tipo</th>
                        <th>Provincia</th>
                        <th class="text-center">Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clubs as $key => $club)
                        <tr class="@if($club->active == 0) card-inactive @endif">
                            <td>{!! $club->uuid !!}</td>
                            <td>{!! $club->name !!}</td>
                            <td>{!! $club->origin !!}</td>
                            <td>{!! $club->type !!}</td>
                            <td>{!! $club->providence !!}</td>
                            <td class="text-center">
                                <div class='btn-group'>
                                    @if($club->active == 1)
                                        <a 
                                            href="{{ route('unactive.club',$club->id) }}"
                                            data-toggle="tooltip" title="Activar club"
                                            data-placement= 'top'
                                             class="btn btn-warning single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/ban.png') }})"></div>
                                        </a>
                                    @endif

                                    @if($club->active == 0)
                                        <a 
                                            href="{{ route('active.club',$club->id) }}" 
                                            data-toggle="tooltip" title="Desactivar club"
                                            data-placement= 'top'
                                             class="btn btn-success single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/success.png') }})"></div>
                                        </a>
                                        
                                    @endif
                                    {{-- @can('edit_clubs') --}}
                                        <a 
                                            data-toggle="tooltip" title="Editar grupo"
                                            data-placement= 'top'
                                            href="{!! route('clubs.edit', [$club->id]) !!}" class='btn btn-info single-button btn-xs'>
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/edit.png') }})"></div>
                                        </a>
                                    {{-- @endcan
                                    @can('delete_clubs') --}}
                                        <button 
                                            data-toggle="tooltip" title="Borrar grupo"
                                            data-placement= 'top'
                                            onclick="return confirm('Estas seguro de eliminar este recurso?')" type="submit" class="btn btn-danger single-button btn-xs">
                                            <div class="icon-basf-small" style="background-image: url({{ asset('img/trash.png') }})"></div>
                                        </button>
                                   {{--  @endcan --}}
                                </div>
                            </td>
                        </tr>
                    @endforeach
                
                </tbody>
            </table>   
        </div>
    </div>
</div>
