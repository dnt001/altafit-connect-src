@foreach($clubs as $key => $club)
    <div class="card-club col-sm-12 element-input mb-3 pd-zero shadow">
        <div class="card-header @if($club->active == 0) card-inactive @endif">
            <h3>
                <label for="" class="title-card">
                    {{ $club->uuid }} - {{ $club->name }} <span class="light">|</span> <small>Razón social: {{ $club->bussines_name }}</small> <span class="light">|</span> <small>CIF: {{ $club->cif }}</small> 
                </label>

                <a 
                    class="btn btn-primary btn-header right dropdown-toggle d-flex align-items-center justify-content-center optionsDropdown" data-id="{{ $key }}" id="optionsDropdown-{{ $key }}" href="#" data-toggle="dropdown"
                    data-toggle="tooltip" data-placement="left" data-original-title="Más opciones">
                   <i class="mdi mdi-dots-horizontal"></i>
                </a>
                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list opts optionsDropdown-{{ $key }}" aria-labelledby="optionsDropdown-{{ $key }}">
                    <a class="dropdown-item preview-item opts-item" href="{{ route('clubs.edit',$club->id) }}">
                        <div class="preview-thumbnail opts-icon">
                            <div class="preview-icon bg-info">
                                <i class="mdi mdi-tooltip-edit mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content opts-content">
                            <h6 class="preview-subject font-weight-normal">Modificar</h6>
                        </div>
                    </a>

                    @if($club->active == 1)
                        <a class="dropdown-item preview-item opts-item" href="{{ route('unactive.club',$club->id) }}">
                            <div class="preview-thumbnail opts-icon">
                                <div class="preview-icon bg-info">
                                    <div class="icon-basf-small" style="background-image: url({{ asset('img/ban.png') }})"></div>
                                </div>
                            </div>
                            <div class="preview-item-content opts-content">
                                <h6 class="preview-subject font-weight-normal">Desactivar</h6>
                            </div>
                        </a>
                    @endif

                    @if($club->active == 0)
                        <a class="dropdown-item preview-item opts-item" href="{{ route('active.club',$club->id) }}">
                            <div class="preview-thumbnail opts-icon">
                                <div class="preview-icon bg-info">
                                    <div class="icon-basf-small" style="background-image: url({{ asset('img/success.png') }})"></div>
                                </div>
                            </div>
                            <div class="preview-item-content opts-content">
                                <h6 class="preview-subject font-weight-normal">Activar</h6>
                            </div>
                        </a>
                    @endif
                    
                    <a class="dropdown-item preview-item opts-item" onclick="showSwal('warning-message-and-cancel',{{ $club->id }})">
                        <div class="preview-thumbnail opts-icon">
                            <div class="preview-icon bg-info">
                                <i class="mdi mdi-close mx-0"></i>
                            </div>
                        </div>
                        <div class="preview-item-content opts-content">
                            <h6 class="preview-subject font-weight-normal">Eliminar</h6>
                        </div>
                    </a>
                </div>
            </h3>
        </div>
        <div class="card-body">
            <ul class="list-unstyled list-horizontal">
                <li>
                    <i class="mdi mdi-map-marker-radius"></i> 
                    <span>Dirección : {{ $club->address }}, {{ $club->postal_code }} </span>
                </li>
                <li>
                    <i class="mdi mdi-city"></i> 
                    <span>Ciudad : {{ $club->city }} </span>
                </li>
                <li>
                    <i class="mdi mdi-flag-checkered"></i> 
                    <span>Provincia : {{ $club->providence }} </span>
                </li>
            </ul>
            

            <ul class="list-unstyled list-horizontal">
                <li>
                    <i class="mdi mdi-account-key"></i> 
                    <span>Director : {{ $club->director }} </span>
                </li>{{-- 
                <li>
                    <i class="mdi mdi-account-multiple-outline"></i> 
                    <span>Coordinador : {{ $club->coordinator }} </span>
                </li> --}}
            </ul>
        </div>

        {{-- <div class="card-footer">
            <ul class="list-unstyled list-horizontal">
                <li>
                    <i class="mdi mdi-lock"></i> 
                    <span>Llave privada : {{ $club->pv_sk }} </span>
                </li>
                <li>
                    <i class="mdi mdi-key-variant"></i> 
                    <span>Llave pública : {{ $club->pv_pk }} </span>
                </li>
                <li>
                    <i class="mdi mdi-numeric-1-box-multiple-outline"></i> 
                    <span>ID Provis : {{ $club->provis_id }} </span>
                </li>
            </ul>
        </div> --}}
    </div>
@endforeach
