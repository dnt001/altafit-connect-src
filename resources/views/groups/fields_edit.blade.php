<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>


@foreach($list as $key => $item)
  <div class="col-sm-4 container-panel-permission">
    <h3>{{$key}}</h3>
    <div class="well">
        <ul class="list-unstyled">
          @foreach($item as $value)
            <li class="list-group-item">
                <div class="form-check">
                  <label class="form-check-label">
                    {!! Form::checkbox('permissions[]', remove_keyword($value.'_'.$key), $groups->hasPermissionTo(remove_keyword($value.'_'.$key)),['class' => 'form-check-input']) !!} 
                    {{translate_keywords($value)}}
                  </label>
                </div>
            </li>
           @endforeach
        </ul>
    </div>
 </div>
@endforeach




<!-- Submit Field -->
<div class="form-group col-sm-12 container-buttons pd-zero">
    {!! Form::submit('Guardar', ['class' => 'right single-button  btn btn-primary']) !!}
    <a href="{!! route('groups.index') !!}" class="right single-button  btn btn-default">Cancelar</a>
</div>
