@extends('layouts.app')

@section('content')
   <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h3>
                            {{ __('Listado de grupos') }}
                            @can('create_groups')
                                <a 
                                    data-toggle="tooltip" title="Crear grupo"
                                    data-placement= 'top'
                                    class="btn btn-primary right single-button btn-xs first-button"  href="{!! route('groups.create') !!}">
                                    <div class="icon-basf" style="background-image: url({{ asset('img/team.png') }})"></div>
                                </a>
                            @endcan
                            @can('view_permissions')
                                <a 
                                    data-toggle="tooltip" title="Listado de permisos"
                                    data-placement= 'top'
                                    class="btn btn-primary right single-button btn-xs"  href="{!! route('permissions.index')  !!}">
                                    <div class="icon-basf" style="background-image: url({{ asset('img/permission.png') }})"></div>
                                </a>
                            @endcan
                        </h3>
                    </div>

                    <div class="card-body">
                        @include('groups.table')
                    </div>

                    <div class="card-footer">
                        <div class="text-center">
        
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

