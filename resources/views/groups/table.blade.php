<div class="table-responsive">
    <table class="table" id="groups-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th colspan="3"></th>
            </tr>
        </thead>
        <tbody>
            @if($groups->isEmpty())
                <tr>
                    <td colspan="5" class="text-center">No hay registros</td>
                </tr>
            @else
                @foreach($groups as $groups)
                    <tr>
                        <td>{!! $groups->name !!}</td>
                        <td class="right-text">
                            {!! Form::open(['route' => ['groups.destroy', $groups->id], 'method' => 'delete' , 'class' => 'form-buttons']) !!}
                            <div class='btn-group'>
                                {{-- @can('view_groups')
                                    <a href="{!! route('groups.show', [$groups->id]) !!}" class='btn btn-default btn-xs'>
                                        <i class="glyphicon glyphicon-eye-open"></i>
                                    </a>
                                @endcan --}}
                                @can('edit_groups')
                                    <a 
                                        data-toggle="tooltip" title="Editar grupo"
                                        data-placement= 'top'
                                        href="{!! route('groups.edit', [$groups->id]) !!}" class='btn btn-info single-button btn-xs'>
                                        <div class="icon-basf-small" style="background-image: url({{ asset('img/edit.png') }})"></div>
                                    </a>
                                @endcan
                                @can('delete_groups')
                                    <button 
                                        data-toggle="tooltip" title="Borrar grupo"
                                        data-placement= 'top'
                                        onclick="return confirm('Estas seguro de eliminar este recurso?')" type="submit" class="btn btn-danger single-button btn-xs">
                                        <div class="icon-basf-small" style="background-image: url({{ asset('img/trash.png') }})"></div>
                                    </button>
                                @endcan
                            </div>
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
            @endif
        
        </tbody>
    </table>   
</div>

