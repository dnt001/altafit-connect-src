<div class="col-sm-12 table-responsive">
 	<table class="table table-bordered table-condensed table-striped ">
	<thead>
		<tr>
		  <th>Titulo</th>
		  <th>Valor</th>
		</tr>
	</thead>
	<tbody>
		<!-- Id Field -->
		<tr>
		  <td>
    		     {!! Form::label('id', 'Id:') !!}
		  </td>
		  <td>
		      <p>{!! $groups->id !!}</p>
		  </td>
		</tr>
		<!-- Name Field -->
		<tr>
		  <td>
   		       {!! Form::label('name', 'Nombre:') !!}
    		  </td>
		  <td>
		       <p>{!! $groups->name !!}</p>
		  </td>
		</tr>

		<!-- Created At Field -->
		<tr>
	 	   <td>
    		        {!! Form::label('created_at', 'Fecha de creacion:') !!}
   		   </td>
		   <td>
		        <p>{!! $groups->created_at !!}</p>
		   </td>
		</tr>
	  </tbody>
	</table>
</div>

<div class="col-sm-12">
	<h2>Listado de permisos asignados</h2>
	@foreach($list as $key => $item)
		<div class="col-xs-4">
			<h3>{{$key}}</h3>
			<div class="well">
					<ul class="list-unstyled">
						@foreach($item as $value)
							<li class="list-group-item">
									 {{translate_keywords($value)}}
							</li>
						 @endforeach
					</ul>
			</div>
	 </div>
	@endforeach
</div>
