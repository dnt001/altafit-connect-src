@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Groups
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('groups.show_fields')
                    <div class="col-sm-12">
                      <a href="{!! route('groups.index') !!}" class="btn btn-default pull-right">Regresar</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
