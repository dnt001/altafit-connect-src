@extends('layouts.front')
@section('content')

<div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth lock-full-bg">
       <div class="row w-100">
          <div class="col-lg-12 mx-auto">
             <div class="auth-form-transparent text-left p-5 text-center">
                <div class="container-image">
                   <img src="../../images/logo-altafit.png" alt="logo" class="img-fluid center-image" />
                </div>
                

                <h1 class="white">
                    Se ha enviado un correo con las siguientes instrucciones. 
                </h1>
                <h1 class="white">
                    Gracias.
                </h1>
             </div>
          </div>
       </div>
    </div>
</div>

@endsection