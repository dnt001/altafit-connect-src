@extends('layouts.app')

@section('header')
    <section class="content-header">
        <h1 class="left hidden-xs hidden-sm">
            <i class="mdi mdi-account-card-details"></i>
            Listado de clubs 
        </h1>

        <h1 class="left hidden-md hidden-lg">
            Clubs 
        </h1>
        <h1 class="right">
            
           <a class="btn btn-primary btn-header search-filters hidden-xs hidden-sm" 
                 data-toggle="tooltip" data-placement="bottom" title="Abrir filtros de busqueda" data-original-title="Abrir filtros de busqueda"
                href="#">
               <i class="mdi mdi-filter"></i>
           </a>
           <a class="btn btn-primary btn-header hidden-xs hidden-sm" 
                 data-toggle="tooltip" data-placement="bottom" title="Dar de alta club" data-original-title="Dar de alta club"
                href="{!! route('clubs.create') !!}">
               <div class="icon-basf-medium" style="background-image: url({{ asset('img/store.png') }})"></div>
           </a>
        </h1>
    </section>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 pd-zero">
                @include('flash::message')
                
                <div class="card search-filters-panel">
                    <div class="card-body">
                        <form action="/clubs" method="GET">
                            <div class="col-sm-4 form-group element-input pd-zero-left">
                                <label for="">Ciudad</label>
                                <select name="city_id" id="" class="form-control">
                                    <option value="">-Selecciona una opción-</option>
                                    @foreach($cities as $value)
                                        <option {{ $value->id == $city ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-4 form-group element-input">
                                <label for="">Provincia</label>
                                <select name="providence_id" id="" class="form-control">
                                    <option value="">-Selecciona una opción-</option>
                                    @foreach($providences as $value)
                                        <option {{ $value->id == $providence ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="col-sm-4 form-group element-input pd-zero-right">
                                <label for="">Director</label>
                                <select name="director_id" id="" class="form-control">
                                    <option value="">-Selecciona una opción-</option>
                                    @foreach($directors as $value)
                                        <option {{ $value->id == $director ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div>

                            {{-- <div class="col-sm-3 form-group element-input pd-zero-right">
                                <label for="">Coordinador</label>
                                <select name="coordinator_id" id="" class="form-control">
                                    <option value="">-Selecciona una opción-</option>
                                    @foreach($coordinators as $value)
                                        <option {{ $value->id == $coordinator ? 'selected' : '' }} value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </div> --}}
                            
                            <div class="col-sm-12 form-group element-input pd-zero">
                                <label for="">Busqueda</label>
                                <input type="text" value="{{ $search }}" class="form-control" name="search">
                            </div>

                            <div class="col-sm-12 form-group element-input pd-zero">
                                <button class="right btn btn-primary">Buscar</button>
                            </div>
                        </form>
                    </div>
                </div>

                @include('clubs.table')

                
            </div>
        </div>
    </div>
@endsection

