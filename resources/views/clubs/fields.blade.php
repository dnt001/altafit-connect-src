@if(Route::is('clubs.create'))
	<club-component :club="[]" :type-component="'create'"></club-component>
@else
	<club-component :club="{{ json_encode($club) }}" :type-component="'edit'"></club-component>	
	{{-- {!! Form::model($club, ['route' => ['clubs.update', $club->id], 'method' => 'patch','files' => true]) !!}
    {!! Form::close() !!} --}}
@endif