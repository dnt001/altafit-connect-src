<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $club->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $club->name !!}</p>
</div>

<!-- Pv Sk Field -->
<div class="form-group">
    {!! Form::label('pv_sk', 'Pv Sk:') !!}
    <p>{!! $club->pv_sk !!}</p>
</div>

<!-- Pv Pk Field -->
<div class="form-group">
    {!! Form::label('pv_pk', 'Pv Pk:') !!}
    <p>{!! $club->pv_pk !!}</p>
</div>

<!-- Uuid Field -->
<div class="form-group">
    {!! Form::label('uuid', 'Uuid:') !!}
    <p>{!! $club->uuid !!}</p>
</div>

<!-- Provis Id Field -->
<div class="form-group">
    {!! Form::label('provis_id', 'Provis Id:') !!}
    <p>{!! $club->provis_id !!}</p>
</div>

<!-- Dns Field -->
<div class="form-group">
    {!! Form::label('dns', 'Dns:') !!}
    <p>{!! $club->dns !!}</p>
</div>

<!-- Port Field -->
<div class="form-group">
    {!! Form::label('port', 'Port:') !!}
    <p>{!! $club->port !!}</p>
</div>

<!-- Suffix Field -->
<div class="form-group">
    {!! Form::label('suffix', 'Suffix:') !!}
    <p>{!! $club->suffix !!}</p>
</div>

<!-- Type Field -->
<div class="form-group">
    {!! Form::label('type', 'Type:') !!}
    <p>{!! $club->type !!}</p>
</div>

<!-- Origin Field -->
<div class="form-group">
    {!! Form::label('origin', 'Origin:') !!}
    <p>{!! $club->origin !!}</p>
</div>

<!-- Bussines Name Field -->
<div class="form-group">
    {!! Form::label('bussines_name', 'Bussines Name:') !!}
    <p>{!! $club->bussines_name !!}</p>
</div>

<!-- Cif Field -->
<div class="form-group">
    {!! Form::label('cif', 'Cif:') !!}
    <p>{!! $club->cif !!}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', 'Address:') !!}
    <p>{!! $club->address !!}</p>
</div>

<!-- Postal Code Field -->
<div class="form-group">
    {!! Form::label('postal_code', 'Postal Code:') !!}
    <p>{!! $club->postal_code !!}</p>
</div>

<!-- City Field -->
<div class="form-group">
    {!! Form::label('city', 'City:') !!}
    <p>{!! $club->city !!}</p>
</div>

<!-- Providence Field -->
<div class="form-group">
    {!! Form::label('providence', 'Providence:') !!}
    <p>{!! $club->providence !!}</p>
</div>

<!-- Director Id Field -->
<div class="form-group">
    {!! Form::label('director_id', 'Director Id:') !!}
    <p>{!! $club->director_id !!}</p>
</div>

<!-- Coordinator Field -->
<div class="form-group">
    {!! Form::label('coordinator', 'Coordinator:') !!}
    <p>{!! $club->coordinator !!}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created At:') !!}
    <p>{!! $club->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated At:') !!}
    <p>{!! $club->updated_at !!}</p>
</div>

