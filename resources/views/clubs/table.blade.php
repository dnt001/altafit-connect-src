@if(!$clubs->isEmpty())
    @if($view == 'card')
        @include('partials.card')
    @else
        @include('partials.table')
    @endif
@else
    <empty-list-component :url="'{!! route('clubs.create') !!}'" :section="'club'">
    </empty-list-component>
@endif

<div class="text-center">
    {{ $clubs->appends(Request::except('page'))->links() }}
</div>