@extends('layouts.app')

@section('header')
    <section class="content-header">
        <h1 class="left hidden-xs hidden-sm">
            <i class="mdi mdi-account-card-details"></i>
            Editar: {{ $club->name }}
        </h1>

        <h1 class="left hidden-md hidden-lg">
            Editar: {{ $club->name }}
        </h1>
        <h1 class="right">
           <a class="btn btn-primary btn-header" 
                data-toggle="tooltip" data-placement="bottom" data-original-title="Regresar al listado"
                href="{!! route('clubs.index') !!}">
               <i class="mdi mdi-keyboard-backspace"></i>
           </a>
        </h1>
    </section>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12 pd-zero">
                @include('flash::message')
                @include('clubs.fields')
            </div>
        </div>
    </div>
@endsection


@section('scripts')
    
@endsection