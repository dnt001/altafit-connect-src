@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Has sido invitado a formar pate del equipo Altafit!
        @endcomponent
    @endslot
{{-- Body --}}
        @slot('subcopy')
            @component('mail::subcopy')
            Hola {{ $data['name'] }} {{ $data['lastname'] }}.
            @endcomponent
            @component('mail::subcopy')
            Usuario: {{ $data['email'] }}
            @endcomponent
            @component('mail::subcopy')
            Contraseña: {{ $data['send'] }}
            @endcomponent
        @endslot


        @component('mail::button', ['url' => $url])
        Ir al sitio
        @endcomponent
{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent