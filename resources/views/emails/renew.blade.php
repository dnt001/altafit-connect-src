@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Recuperación de contraseña
        @endcomponent
    @endslot
{{-- Body --}}
        @slot('subcopy')
            @component('mail::subcopy')
            Hola {{ $data['name'] }} {{ $data['lastname'] }}.
            @endcomponent
            @component('mail::subcopy')
            Tu contraseña ha sido modificada, tus credenciales de acceso son las siguientes.
            @endcomponent
            @component('mail::subcopy')
            Usuario: {{ $data['email'] }}
            @endcomponent
            @component('mail::subcopy')
            Contraseña: {{ $data['send'] }}
            @endcomponent

            @component('mail::button', ['url' => $url])
            Ir al sitio
            @endcomponent

            @component('mail::subcopy')
            Al ingresar al sitio, por favor coloca tu nueva contraseña. Gracias.
            @endcomponent
    
        @endslot

{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent