@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => config('app.url')])
            Recuperación de contraseña
        @endcomponent
    @endslot
{{-- Body --}}
        @slot('subcopy')
            @component('mail::subcopy')
            Hola {{ $user->name }} {{ $user->profile->lastname }}.
            @endcomponent
            @component('mail::subcopy')
            Recibes este e-mail porque hemos registrado una solicitud de cambio de contraseña en tu cuenta de Altafit-connect..
            @endcomponent
            @component('mail::button', ['url' => $url])
            Cambiar contraseña
            @endcomponent
            @component('mail::subcopy')
            Este enlace para cambiar tu contraseña caduca en 60 minutos.
            @endcomponent
            @component('mail::subcopy')
            Si no has solicitado un cambio de contraseña, no tienes que hace nada.
            @endcomponent
        @endslot

        @slot('subcopy')
            @component('mail::subcopy')
            Saludos.
            @endcomponent
            @component('mail::subcopy')
            Altafit-connect.
            @endcomponent
        @endslot

{{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} {{ config('app.name') }}.
        @endcomponent
    @endslot
@endcomponent