<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>{{ config('app.name', 'Altafit') }}</title>

        <link href="{{ asset('css/template.css') }}" rel="stylesheet">
        <link href="{{ asset('css/front.css') }}" rel="stylesheet">

        {{-- <link href="https://altafitgymclub.com/wp-content/themes/altafit-theme/images/isotipo.png" rel="shortcut icon" sizes="16x16 32x32 64x64"> --}}

        <link rel="stylesheet" href=" {{ asset('vendors/mdi/css/materialdesignicons.min.css')}}">
        <link rel="stylesheet" href=" {{ asset('vendors/css/vendor.bundle.base.css')}}">
        <link rel="stylesheet" href=" {{ asset('vendors/flag-icon-css/css/flag-icon.min.css')}}">

        <link rel="stylesheet" href="{{ asset('css/screen.css') }}">
        
        
    </head>
    <body>
        <div class="container-scroller" id="app">
            <div id="loader-wrapper">
                <div id="loader"></div>
                <div class="loader-section section-left"></div>
                <div class="loader-section section-right"></div>
            </div>
            @yield('content')
        </div>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

        <script src="{{ asset('vendors/js/vendor.bundle.base.js')}}"></script>

        <script src="{{ asset('js/off-canvas.js')}}"></script>
        <script src="{{ asset('js/hoverable-collapse.js')}}"></script>
        <script src="{{ asset('js/template.js')}}"></script>

        <script src="{{ asset('js/modernizr.js') }}"></script>
        <script src="{{ asset('js/screen.js') }}"></script>
    </body>

</html>