<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
            <meta charset="utf-8">
            <meta name="viewport" content="width=device-width, initial-scale=1">

            <!-- CSRF Token -->
            <meta name="csrf-token" content="{{ csrf_token() }}">

            <title>{{ config('app.name', 'Pharma') }}</title>

            <!-- Scripts -->
            <!-- Fonts -->
            <link rel="dns-prefetch" href="//fonts.gstatic.com">
            <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.3.5/jquery.fancybox.css">
            <link rel="stylesheet" 
                  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css">
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.5/css/select2.min.css">
            <link rel="stylesheet" 
                  href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-colorpicker/2.5.3/css/bootstrap-colorpicker.min.css">
            <!-- Styles -->
            <link href="{{ asset('css/app.css') }}" rel="stylesheet">
            
            <link href="{{ asset('css/template.css') }}" rel="stylesheet">

            <link rel="stylesheet" href=" {{ asset('vendors/mdi/css/materialdesignicons.min.css')}} ">
            <link rel="stylesheet" href=" {{ asset('vendors/css/vendor.bundle.base.css')}} ">
            <link rel="stylesheet" href=" {{ asset('vendors/flag-icon-css/css/flag-icon.min.css')}} ">
            <link rel="stylesheet" href="{{ asset('vendors/simplemde/simplemde.min.css')}}">
            {{-- <link href="https://altafitgymclub.com/wp-content/themes/altafit-theme/images/isotipo.png" rel="shortcut icon" sizes="16x16 32x32 64x64"> --}}
            <link rel="stylesheet" href="{{ asset('vendors/bootstrap-formhelpers/css/bootstrap-formhelpers.min.css')}}">
    </head>
    <body class="sidebar-icon-only">
        <div class="container-scroller" id="app">
            <!-- partial:partials/_navbar.html -->
            <nav class="navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
                <div class="text-left navbar-brand-wrapper d-flex align-items-center justify-content-between">
                    <a class="navbar-brand brand-logo" href="/home">
                        <img src="../../images/logo-altafit.png" alt="logo" />
                    </a>
                    <a class="navbar-brand brand-logo-mini" href="/home">
                        {{-- <img src="" alt="logo" /> --}}
                    </a>
                    <button class="navbar-toggler align-self-center" type="button" data-toggle="minimize">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
                <div class="navbar-menu-wrapper d-flex align-items-center justify-content-end">
                    <ul class="navbar-nav header-title">
                        @yield('header')
                    </ul>


                    <ul class="navbar-nav navbar-nav-right">
                        @if(Route::is('clubs.index'))
                            <li class="nav-item dropdown btn-nav nav-user-icon">
                                <a class="btn btn-primary btn-header nav-link count-indicator dropdown-toggle  align-items-center justify-content-center" id="clubDropdown" href="#" data-toggle="dropdown">
                                     <i class="mdi mdi-dots-horizontal"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list clubDropdown" aria-labelledby="clubDropdown">

                                    <a class="dropdown-item preview-item" href="{{ route('export') }}">

                                        <div class="preview-thumbnail">
                                            <div class="preview-icon bg-info">
                                                <i class="mdi mdi-file-export mx-0"></i>
                                            </div>
                                        </div>
                                        <div class="preview-item-content">
                                            <h6 class="preview-subject font-weight-normal">Exportar</h6>
                                        </div>
                                        
                                    </a>

                                    <a class="dropdown-item preview-item" href="/clubs?view=card">

                                        <div class="preview-thumbnail">
                                            <div class="preview-icon bg-info">
                                                <i class="mdi mdi-credit-card mx-0"></i>
                                            </div>
                                        </div>
                                        <div class="preview-item-content">
                                            <h6 class="preview-subject font-weight-normal">Ver como carta</h6>
                                        </div>
                                        
                                    </a>

                                    <a class="dropdown-item preview-item" href="/clubs?view=table">

                                        <div class="preview-thumbnail">
                                            <div class="preview-icon bg-info">
                                                <i class="mdi mdi-table-large mx-0"></i>
                                            </div>
                                        </div>
                                        <div class="preview-item-content">
                                            <h6 class="preview-subject font-weight-normal">Ver como tabla</h6>
                                        </div>
                                        
                                    </a>
                                    <a class="dropdown-item preview-item hidden-md hidden-lg search-filters" href="#">

                                        <div class="preview-thumbnail">
                                            <div class="preview-icon bg-info">
                                                <i class="mdi mdi-filter mx-0"></i>
                                            </div>
                                        </div>
                                        <div class="preview-item-content">
                                            <h6 class="preview-subject font-weight-normal">Buscar</h6>
                                        </div>
                                        
                                    </a>

                                    <a class="dropdown-item preview-item hidden-md hidden-lg" href="{!! route('clubs.create') !!}">

                                        <div class="preview-thumbnail">
                                            <div class="preview-icon bg-info">
                                                <div class="icon-basf-medium" style="background-image: url({{ asset('img/store.png') }})"></div>
                                            </div>
                                        </div>
                                        <div class="preview-item-content">
                                            <h6 class="preview-subject font-weight-normal">Crear club </h6>
                                        </div>
                                        
                                    </a>
                                </div>
                            </li>
                        @endif
                        
                        @if(Route::is('home'))
                            <li class="nav-item dropdown  nav-user-icon">
                                <a class="btn btn-primary btn-header nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="clubDropdown" href="#" data-toggle="dropdown">
                                     <img src="{{ asset('img/images-w-2.png')}}" alt="profile" />
                                </a>
                                <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list clubDropdown" aria-labelledby="clubDropdown">
                                    <input type="text" class="form-control" placeholder="Buscar...">
                                    @foreach(Auth::user()->getClubs() as $key => $clubs)
                                        <a class="dropdown-item preview-item" href="{{ route('selected',$key) }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('selected-form-'{{ $key }}).submit();">

                                            <div class="preview-thumbnail">
                                                <div class="preview-icon bg-info">
                                                    <div class="icon-basf-medium" style="background-image: url({{ asset('img/images-w.png') }})"></div>
                                                </div>
                                            </div>
                                            <div class="preview-item-content">
                                                <h6 class="preview-subject font-weight-normal">
                                                    {{ $clubs }}
                                                </h6>
                                            </div>
                                            
                                        </a>

                                        <form id="selected-form-{{ $key }}" action="{{ route('selected',$key) }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    @endforeach
                                </div>
                            </li>
                        @endif
                        <li class="nav-item dropdown  nav-user-icon">
                            <a class="nav-link count-indicator dropdown-toggle d-flex align-items-center justify-content-center" id="notificationDropdown" href="#" data-toggle="dropdown">
                                 <img src="{{ asset('img/man.png')}}" alt="profile" />
                            </a>
                            <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list notificationDropdown" aria-labelledby="notificationDropdown">
                                <a class="dropdown-item preview-item" href="/groups">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-lock mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-normal">Grupos y permisos</h6>
                                    </div>
                                </a>
                                
                                <a class="dropdown-item preview-item" href="/users">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-account-multiple mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-normal">Usuarios</h6>
                                    </div>
                                </a>
                                <a class="dropdown-item preview-item" href="#" data-toggle="modal" data-target="#exampleModal">
                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-account-settings mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-normal">Cambiar contraseña</h6>
                                    </div>
                                </a>
                                <a class="dropdown-item preview-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">

                                    <div class="preview-thumbnail">
                                        <div class="preview-icon bg-info">
                                            <i class="mdi mdi-logout-variant mx-0"></i>
                                        </div>
                                    </div>
                                    <div class="preview-item-content">
                                        <h6 class="preview-subject font-weight-normal">{{ __('Cerrar sesión') }}</h6>
                                    </div>
                                    
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    </ul>
                    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
                        <span class="mdi mdi-menu"></span>
                    </button>
                </div>
            </nav>
            <!-- partial -->
            <div class="container-fluid page-body-wrapper">
                
                <!-- partial:partials/_sidebar.html -->
                <nav class="sidebar sidebar-offcanvas" id="sidebar">
                    <ul class="nav">
                        <li class="nav-item nav-profile">
                            <div class="nav-link d-flex">
                                <div class="profile-name">
                                    <p class="name">{{ Auth::user()->name }}</p>
                                    <p class="designation">{{ Auth::user()->roles[0]->name }}</p> 
                                </div>
                            </div>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/home">
                                <i class="mdi mdi-chart-bar menu-icon"></i>
                                <span class="menu-title">Finanzas</span>
                            </a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="/clubs">
                                <i class="mdi mdi-store menu-icon"></i>
                                <span class="menu-title">Clubs</span>
                            </a>
                        </li>

                        
                        
                    </ul>
                </nav>
                <!-- partial -->
                <div class="main-panel">
                    <div class="content-wrapper">
                        <div class="row">
                            <div class="col-md-12">
                                    @yield('content')
                            </div>
                        </div>
                    </div>
                    <!-- content-wrapper ends -->
                    <!-- partial:partials/_footer.html -->
                    <div class="footer-wrapper">
                        <footer class="footer">
                            <div class="d-sm-flex justify-content-center justify-content-sm-between">
                                <span class="text-center text-sm-left d-block d-sm-inline-block">&copy; {{ date('Y') }}. All rights reserved. </span>
                            </div>
                        </footer>
                    </div>
                </div>
            <!-- page-body-wrapper ends -->
            </div>
        </div>
        
        <div class="modal fade in show" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Cambiar contraseña</h5>
                        @if(Auth::user()->first_login == 1)
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        @endif
                    </div>
                    {!! Form::open(['route' => ['change.password'], 'method' => 'post','files' => true]) !!}
                        <div class="modal-body">
                            <div class="col-sm-12 form-group element-input">
                                {!! Form::label('name', 'Contraseña nueva:') !!}
                                <input type="password" name="password" class="form-control">
                            </div>

                            <div class="col-sm-12 form-group element-input">
                                {!! Form::label('name', 'Repetir contraseña:') !!}
                                <input type="password" name="repeat_password" class="form-control">
                            </div>
                            <div style="clear:both;"></div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-success">Cambiar</button>
                        </div>
                    {!! Form::close() !!}
                    
                </div>
            </div>
        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/locale/es.js"></script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCzK2iI66bP2BCdH9lg5FBT1IBuXx7KXyk&libraries=places">
        </script>

        <script src="{{ asset('vendors/js/vendor.bundle.base.js')}}"></script>

        <script src="{{ asset('js/off-canvas.js')}}"></script>
        <script src="{{ asset('js/hoverable-collapse.js')}}"></script>
        <script src="{{ asset('js/template.js')}}"></script>

        <!-- endinject -->
        <!-- Plugin js for this page-->
        <script src="{{ asset('vendors/progressbar.js/progressbar.min.js')}}"></script>
        <script src="{{ asset('vendors/flot/jquery.flot.js')}}"></script>
        <script src="{{ asset('vendors/flot/jquery.flot.resize.js')}}"></script>
        <script src="{{ asset('vendors/flot/curvedLines.js')}}"></script>
        <script src="{{ asset('vendors/chart.js/Chart.min.js')}}"></script>
        <script src="{{ asset('js/file-upload.js')}}"></script>
        <script src="{{ asset('vendors/simplemde/simplemde.min.js')}}"></script>

        <script src="{{ asset('vendors/bootstrap-formhelpers/js/bootstrap-formhelpers.min.js')}}"></script>
        <script src="{{ asset('js/site.js') }}" defer></script>
        <!-- End plugin js for this page -->
        <!-- Custom js for this page-->
        

        <!-- plugin js for this page -->
        <script src="{{ asset('vendors/sweetalert/sweetalert.min.js')}}"></script>
        <script src="{{ asset('vendors/jquery.avgrund/jquery.avgrund.min.js')}}"></script>
        <!-- End plugin js for this page -->
        <!-- Custom js for this page-->
        <script src="{{ asset('js/alerts.js')}}"></script>


        @yield('scripts')

        <script>
            $(document).ready(function(){
                let is_first_login = '{{ Auth::user()->first_login }}';

                if(is_first_login == 0){
                    $('#exampleModal').modal({
                        backdrop: 'static',
                        keyboard: false
                    });
                    $('.modal-backdrop').toggleClass('show')
                }
            })
        </script>
        <!-- endinject -->
        <!-- Custom js for this page-->{{-- 
        <script src="{{ asset('js/dashboard.js')}}"></script> --}}
    </body>
    
</html>