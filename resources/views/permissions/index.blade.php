@extends('layouts.app')

@section('content')
   <div class="container-fluid">
        <div class="row justify-content-center">
            <div class="col-md-12">
                @include('flash::message')
                <div class="card">
                    <div class="card-header">
                        <h3>
                            {{ __('Listado de permisos') }}
                            @can('create_permissions')
                                <a 
                                    data-toggle="tooltip" title="Crear permisos"
                                    data-placement= 'top'
                                    class="btn btn-primary right single-button btn-xs"  href="{!! route('permissions.create')  !!}">
                                    <div class="icon-basf" style="background-image: url({{ asset('img/permission.png') }})"></div>
                                </a>
                            @endcan
                        </h3>
                    </div>

                    <div class="card-body">
                        @include('permissions.table')
                    </div>

                    <div class="card-footer">
                        <div class="text-center">
                            {{ $permissions->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
