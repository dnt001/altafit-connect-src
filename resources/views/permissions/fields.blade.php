<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Nombre:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12 pd-zero">
    {!! Form::submit('Guardar', ['class' => 'btn right single-button btn-primary']) !!}
    <a href="{!! route('permissions.index') !!}" class="btn right single-button  btn-default">Cancelar</a>
</div>
