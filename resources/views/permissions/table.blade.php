<div class="table-responsive">
    <table class="table" id="permissions-table">
        <thead>
            <tr>
                <th>Nombre</th>
                <th colspan="3"></th>
            </tr>
        </thead>
        <tbody>
        @if($permissions->isEmpty())
        @else
            @foreach($permissions as $permission)
                <tr>
                    <td>{!! $permission->name !!}</td>
                    <td class="right-text">
                        {!! Form::open(['route' => ['permissions.destroy', $permission->id], 'method' => 'delete', 'class'=> 'form-buttons']) !!}
                            <div class='btn-group'>
                                @can('edit_permissions')
                                    <a 
                                        data-toggle="tooltip" title="Editar permiso"
                                        data-placement= 'top'
                                        href="{!! route('permissions.edit', [$permission->id]) !!}" class='btn btn-info single-button btn-xs'>
                                        <div class="icon-basf-small" style="background-image: url({{ asset('img/edit.png') }})"></div>
                                    </a>
                                @endcan
                                @can('delete_permissions')
                                    <button 
                                        data-toggle="tooltip" title="Borrar permiso"
                                        data-placement= 'top'
                                        onclick="return confirm('Estas seguro de eliminar este recurso?')" type="submit" class="btn btn-danger single-button btn-xs">
                                        <div class="icon-basf-small" style="background-image: url({{ asset('img/trash.png') }})"></div>
                                    </button>
                                @endcan
                            </div>
                        {!! Form::close() !!}
                    </td>
                </tr>
            @endforeach
        @endif
        </tbody>
    </table>   
</div>

