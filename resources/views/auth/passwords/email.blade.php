@extends('layouts.front')
@section('content')

<div class="container-fluid page-body-wrapper full-page-wrapper">
    <div class="content-wrapper d-flex align-items-center auth lock-full-bg">
       <div class="row w-100">
          <div class="col-lg-4 mx-auto">
             <div class="auth-form-transparent text-left p-5 text-center">
                <div class="container-image">
                   <img src="../../images/logo-altafit.png" alt="logo" class="img-fluid center-image" />
                </div>
                <h5 class="font-weight-light text-center mb-2 white">Introduzca su dirección de email para cambiar la contraseña</h5>

                <form method="post" action="{{ route('reset.altafit') }}">
                    {!! csrf_field() !!}

                     <div class="form-group">
                        <h3 class="font-weight-light text-center mb-2 white">Correo electrónico.</h3>
                        <input type="email" class="form-control text-center white-form" name="email" id="email" placeholder="Correo electrónico">
                     </div>

                    @if (session('status') == 401)
                        <h4 class="font-weight-light white">{{ session('errors') }}</h4>
                    @endif

                    @if (session('status') == 200)
                        <h4 class="font-weight-light white">{{ session('errors') }}</h4>
                    @endif
                     <div class="mt-5">
                        <button class="btn btn-block btn-success btn-lg font-weight-medium" href="">Recuperar contraseña</button>
                     </div>
                     <div class="mt-3 text-center">
                        <a href="/" class="auth-link text-white">Ir a iniciar sesión</a>
                     </div>
                </form>
             </div>
          </div>
       </div>
    </div>
</div>

@endsection