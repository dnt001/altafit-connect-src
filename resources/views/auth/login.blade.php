@extends('layouts.front')

@section('content')
    <div class="container-fluid page-body-wrapper full-page-wrapper">
      <div class="content-wrapper d-flex align-items-stretch auth auth-img-bg bg-dark">
        <div class="row flex-grow">
          <div class="col-lg-6 d-flex align-items-center justify-content-center">
            <div class="auth-form-transparent text-left p-3">
              <div class="brand-logo">
                {{-- <img src="{{ asset('images/logo-geometry.png')}}" alt="logo"> --}}
              </div>
              
              <div class="container-image">
                <img src="../../images/logo-altafit.png" alt="logo" class="img-fluid center-image" />
              </div>
              <h3 class="font-weight-light text-center mb-2">Iniciar sesión.</h3>

              <form method="POST" action="{{ route('login.altafit') }}" class="pt-3">
                {{ csrf_field() }}
                <div class="form-group">
                  <label for="exampleInputEmail">Correo electrónico</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-account-outline text-primary"></i>
                      </span>
                    </div>
                    <input id="email" type="email" class="form-control form-control-lg border-left-0" name="email" value="{{ old('email') }}" placeholder="Correo electrónico" required autofocus>
                  </div>
                </div>
                <div class="form-group">
                  <label for="exampleInputPassword">Contraseña</label>
                  <div class="input-group">
                    <div class="input-group-prepend bg-transparent">
                      <span class="input-group-text bg-transparent border-right-0">
                        <i class="mdi mdi-lock-outline text-primary"></i>
                      </span>
                    </div>

                    <input id="password" type="password" class="form-control form-control-lg border-left-0" name="password" placeholder="Contraseña" required>                    
                  </div>
                </div>
                <div class="row mb-4">
                  <div class="col error-class">
                      {{ $errorsLogin }}
                  </div>
                </div>
                <div class="my-2 d-flex justify-content-between align-items-center">
                  <div class="form-check">
                    <label class="form-check-label text-muted">
                      <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                      {{ __('Recuerdame') }}
                    </label>
                  </div>
                  @if (Route::has('password.request'))
                    <a class="auth-link text-black" href="{{ route('password.request') }}">
                        {{ __('Has olvidado tu contraseña?') }}
                    </a>
                @endif
                </div>
                <div class="my-3">
                  <button type="submit" 
                    class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btn" >Iniciar sesión</button>
                </div>
              </form>
            </div>
          </div>
          <div class="col-lg-6 login-half-bg d-flex flex-row">
            <p class="text-white font-weight-medium text-center flex-grow align-self-end">Copyright &copy; {{ date('Y') }}  All rights reserved.</p>
          </div>
        </div>
      </div>
      <!-- content-wrapper ends -->
    </div>
    <!-- page-body-wrapper ends -->
  </div>
  <!-- container-scroller -->
  <!-- base:js -->
  <script src="{{ asset('vendors/js/vendor.bundle.base.js')}}"></script>
  <!-- endinject -->
  <!-- inject:js -->
  <script src="{{ asset('js/off-canvas.js')}}"></script>
  <script src="{{ asset('js/hoverable-collapse.js')}}"></script>
  <script src="{{ asset('js/template.js')}}"></script>
  <script src="{{ asset('js/settings.js')}}"></script>
  <script src="{{ asset('js/todolist.js')}}"></script>
  <!-- endinject -->
</body>

</html>




