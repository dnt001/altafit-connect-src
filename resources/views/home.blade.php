@extends('layouts.app')

@section('header')
    <section class="content-header">
        <h1 class="left hidden-xs hidden-sm">
            <i class="mdi mdi-chart-bar"></i>
            Finanzas
        </h1>

        <h1 class="left hidden-md hidden-lg">
            Finanzas 
        </h1>
    </section>
@endsection

@section('content')
	@include('flash::message')
	      <ul class="nav nav-tabs" role="tablist">
	         <li class="nav-item">
	            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#export" role="tab" aria-controls="export" aria-selected="true">Exportación contable.</a>
	         </li>
	         <li class="nav-item">
	            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#emit" role="tab" aria-controls="emit" aria-selected="false"> Remesas de recibos emitidos.</a>
	         </li>
	         <li class="nav-item">
	            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#returned" role="tab" aria-controls="returned" aria-selected="false">Remesas de recibos devueltos.</a>
	         </li>
	         <li class="nav-item">
	            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#result" role="tab" aria-controls="result" aria-selected="false">Cuenta de resultados.</a>
	         </li>
	      </ul>
	      <div class="tab-content">
	         <div class="tab-pane fade show active" id="export" role="tabpanel" aria-labelledby="home-tab">
	            <h4>Exportación contable.</h4>
	         </div>
	         <div class="tab-pane fade" id="emit" role="tabpanel" aria-labelledby="profile-tab">
	            <h4>Remesas de recibos emitidos.</h4>
	         </div>
	         <div class="tab-pane fade" id="returned" role="tabpanel" aria-labelledby="contact-tab">
	            <h4>Remesas de recibos devueltos. </h4>
	            
	         </div>
	         <div class="tab-pane fade" id="result" role="tabpanel" aria-labelledby="contact-tab">
	            <h4>Cuenta de resultados. </h4>
	            
	         </div>
	      </div>
@endsection
