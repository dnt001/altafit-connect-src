(function($) {
  'use strict';
  $(function() {
    var todoListItem = $('.todo-list');
    var todoListInput = $('.todo-list-input');
    $(document).on("click",'.todo-list-add-btn', function(event) {
      
      event.preventDefault();
      let counter = $('.todo-list li').length;
      var item = $(this).prevAll('.todo-list-input').val();

      if (item) {
        todoListItem.append('<input class="hidden" name="task['+counter+'] value="'+item+'">')
        todoListItem.append("<li><div class='form-group col-sm-10 element-input'><span class='form-check-label'>" + item + "</span></div><i class='remove mdi mdi-delete'></i></li>");
        todoListInput.val("");
      }
    });

    todoListItem.on('change', '.checkbox', function() {
      if ($(this).attr('checked')) {
        $(this).removeAttr('checked');
      } else {
        $(this).attr('checked', 'checked');
      }

      $(this).closest("li").toggleClass('completed');

    });

    todoListItem.on('click', '.remove', function() {
      $(this).parent().remove();
    });

  });
})(jQuery);