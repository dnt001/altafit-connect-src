function toggleBounce() {
  if (marker.getAnimation() !== null) {
    marker.setAnimation(null);
  } else {
    marker.setAnimation(google.maps.Animation.BOUNCE);
  }
}



function geocodePosition(pos) 
{
   geocoder = new google.maps.Geocoder();
   geocoder.geocode
    ({
        latLng: pos
    }, 
        function(results, status) 
        {
            if (status == google.maps.GeocoderStatus.OK) 
            {
                $('.lat').val(results[0].geometry.location.lat());
                $('.lng').val(results[0].geometry.location.lng());
                $('#pac-input').val(results[0].formatted_address)
            } 
            else 
            {
                console.log(status)
            }
        }
    );
}

function initMap() {
    var lat = $('.lat').data('lat');
    var lng = $('.lng').data('lng');
    var position = {lat:parseFloat(lat) , lng:parseFloat(lng)};

    var map = new google.maps.Map(document.getElementById('map'), {
      center: position,
      zoom: 13
    });

    var input = document.getElementById('pac-input');


    var autocomplete = new google.maps.places.Autocomplete(input);

    // Bind the map's bounds (viewport) property to the autocomplete object,
    // so that the autocomplete requests use the current map bounds for the
    // bounds option in the request.
    autocomplete.bindTo('bounds', map);

    // Set the data fields to return when the user selects a place.
    autocomplete.setFields(
        ['address_components', 'geometry', 'icon', 'name']);

    var infowindow = new google.maps.InfoWindow();
    var infowindowContent = document.getElementById('infowindow-content');
    infowindow.setContent(infowindowContent);

    var marker = new google.maps.Marker({
      position:position,
      draggable: true,
      animation: google.maps.Animation.DROP,
      map: map,
      anchorPoint: new google.maps.Point(0, -29)
    });

    google.maps.event.addListener(marker, 'dragend', function() {
        geocodePosition(marker.getPosition());
    });

    autocomplete.addListener('place_changed', function() {
      infowindow.close();
      marker.setVisible(false);
      var place = autocomplete.getPlace();
      if (!place.geometry) {
        // User entered the name of a Place that was not suggested and
        // pressed the Enter key, or the Place Details request failed.
        console.log("No details available for input: '" + place.name + "'");
        return;
      }

      $('.lat').val(place.geometry.location.lat())
      $('.lng').val(place.geometry.location.lng())

      // If the place has a geometry, then present it on a map.
      if (place.geometry.viewport) {
        map.fitBounds(place.geometry.viewport);
      } else {
        map.setCenter(place.geometry.location);
        map.setZoom(17);  // Why 17? Because it looks good.
      }
      marker.setPosition(place.geometry.location);
      marker.setVisible(true);

      var address = '';
      if (place.address_components) {
        address = [
          (place.address_components[0] && place.address_components[0].short_name || ''),
          (place.address_components[1] && place.address_components[1].short_name || ''),
          (place.address_components[2] && place.address_components[2].short_name || '')
        ].join(' ');
      }

      infowindowContent.children['place-icon'].src = place.icon;
      infowindowContent.children['place-name'].textContent = place.name;
      infowindowContent.children['place-address'].textContent = address;
      infowindow.open(map, marker);
    });
}

$(document).ready(function(){

  $(document).on('click','.search-filters',function(){
    $('.search-filters-panel').toggleClass('open-search')
  })
  
	$('[data-toggle="tooltip"]').tooltip();   


	$(".product a").fancybox({ 
	    animationEffect : 'fade'
	}).attr('data-fancybox', 'group1');

	var date = new Date();
 	date.setDate(date.getDate()-1);

  $('.datepicker-campaign').datepicker({
	      language:'es',
	      format:'yyyy-mm-dd',
	      startDate: date
	});

  $('.date').datepicker({
        language:'es',
        format:'yyyy-mm-dd'
  });

  

	$(".select2").select2({
      width:'100%',
      language:'es'
    });
});