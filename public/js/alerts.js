(function($) {
  

  showSwal = function(type,id) {
    'use strict';
    if (type === 'warning-message-and-cancel') {
      swal({
        title: 'Quieres eliminar este club?',
        text: "Esta acción no se podra revertir!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3f51b5',
        cancelButtonColor: '#ff4081',
        confirmButtonText: 'Great ',
        buttons: {
          cancel: {
            text: "Cancel",
            value: false,
            visible: true,
            className: "btn btn-danger",
            closeModal: true,
          },
          confirm: {
            text: "OK",
            value: true,
            visible: true,
            className: "btn btn-primary",
            closeModal: true
          }
        }
      }).then(function(result){
          if(result == true){
            let url = 'delete-club'

            axios.defaults.headers.common['X-CSRF-TOKEN'] = $('meta[name="csrf-token"]').attr('content');

            axios.post(url, {id:id})
              .then(function (response) {
                  swal({
                    title:"Eliminado correctamente!", 
                    text:"El club se ha eliminado correctamente!", 
                    icon: 'success',
                    buttons: {
                      confirm: {
                        text: "OK",
                        value: true,
                        visible: true,
                        className: "btn btn-primary",
                        closeModal: true
                      }
                    }
                  }).then(function(result){
                      if(result == true){
                        location.reload();
                      }
                   });
              })
          }
       });

    } 
  }

})(jQuery);